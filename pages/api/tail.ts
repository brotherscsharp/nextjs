// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

type Tail = {
  id: number;
  title: string;
  description: string;
};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Tail>
) {
  res.status(200).json({
    id: 1,
    title: "Hello",
    description: "World",
  });
}
