export default function tailHandler(req, res) {
  let tails = require("../../../resources/tails.json");
  const {
    query: { id, name },
    method,
  } = req;

  switch (method) {
    case "GET":
      // Get data from your database
      let tail = tails.find((t) => t.id == id);
      res.status(200).json({ id, name: `Tail ${tail.title}` });
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
}
