import { useRouter } from "next/router";
import useSwr from "swr";

const fetcher = (url) => fetch(url).then((res) => res.json());

export default function Tail() {
  const router = useRouter();
  const { data, error } = useSwr(
    router.query.id ? `/api/tail/${router.query.id}` : null,
    fetcher
  );

  if (error) return "Failed to load tail";
  if (!data) return "<div>Loading...</div>";

  return `${data.name}`;
}
